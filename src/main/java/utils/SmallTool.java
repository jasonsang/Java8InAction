package utils;

import lombok.extern.slf4j.Slf4j;

import java.util.StringJoiner;


public class SmallTool {
    public SmallTool() {

    }
    public static void sleepMillis(long millis){
        try{
            Thread.sleep(millis);
        } catch (Throwable throwable) {
            System.out.println(throwable.getMessage());
        }

    }

    public static void printTimeAndThread(String tag){
        String result = new StringJoiner("\t|\t")
                .add(String.valueOf(System.currentTimeMillis()))
                .add(String.valueOf(Thread.currentThread().getId()))
                .add(Thread.currentThread().getName())
                .add(tag)
                .toString();
        System.out.println(result);
        }

}
