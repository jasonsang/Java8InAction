package lambdasinaction.chap11;

import utils.SmallTool;

import java.util.concurrent.CompletableFuture;

public class SupplyAsync {
    public static void main(String[] args) {
        SmallTool.printTimeAndThread("进入");
//        SmallTool.printTimeAndThread("选择");
//        CompletableFuture<String> cf1 = CompletableFuture.supplyAsync(() -> {
//            SmallTool.printTimeAndThread("开始");
//            SmallTool.sleepMillis(200);
//            SmallTool.printTimeAndThread("使用");
//            SmallTool.sleepMillis(100);
//
//            return "结束";
//        });
//        CompletableFuture<String> race = CompletableFuture.supplyAsync(()->{
//            SmallTool.printTimeAndThread("处理");
//            SmallTool.sleepMillis(300);
//            return "处理结束";
//        });


        SmallTool.printTimeAndThread("选择");
        CompletableFuture<String> cf1 = CompletableFuture.supplyAsync(() -> {
            SmallTool.printTimeAndThread("开始");
            SmallTool.sleepMillis(200);
//            SmallTool.printTimeAndThread("使用");
//            SmallTool.sleepMillis(100);

            return "结束";
            /*themCompose连接两个异步事件，返回第二个*/
            /*themCombine合并两个异步事件*/
        }).thenCombine(CompletableFuture.supplyAsync(()->{
            SmallTool.printTimeAndThread("加工");
            SmallTool.sleepMillis(300);
            return "";
        }),(d,r)->{
            SmallTool.printTimeAndThread("合成");
            return String.format("%s + %S 好了",d,r);
        });



        SmallTool.printTimeAndThread("提交");
        SmallTool.printTimeAndThread(String.format("%s,就绪",cf1.join()));

    }
}
