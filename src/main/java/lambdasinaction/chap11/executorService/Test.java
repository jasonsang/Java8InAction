package lambdasinaction.chap11.executorService;

import org.junit.rules.Timeout;
import utils.SmallTool;

import java.util.ArrayList;
import java.util.concurrent.*;

public class Test {

    /**
     * 三个任务，每个任务执行时间分别是 10s、3s、6s 。通过 JDK 线程池的 submit 提交这三个 Callable 类型的任务。
     * <p>
     * 第一步：主线程把三个任务提交到线程池里面去，把对应返回的 Future 放到 List 里面存起来，然后执行“都通知完了,等着接吧。”这行输出语句；
     * <p>
     * 第二步：在循环里面执行 future.get() 操作，阻塞等待。
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
//        test1();
        /*创建ExecutorService通过它你可以向线程池提交任务*/
        ExecutorService executorService = Executors.newCachedThreadPool();
        /*向ExecutorService提交一个Callable对象*/
        Future<Double> f = executorService.submit(new Callable<Double>() {
            public Double call() {
                return doSomeLongComputation();
            }


        });
  /*异步其他事情*/
//        doSometingElse();
        f.get(1,TimeUnit.SECONDS);
        System.out.println("ok");
        /*Future的get()方法获取操作结果*/
//        Double result = f.get(1, TimeUnit.SECONDS);
        /*计算抛出一个异常*/
        /*ExecutionException*/
        /*当前线程在等待过程中被中断*/
        /*InterruptedException*/
        /*在Future对象完成之前超时已过期*/
        /*TimeOutException*/

    }
    private static Double doSomeLongComputation() {
        SmallTool.sleepMillis(1000);
        return 1000.00;
    }
    public static void test1() throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        ArrayList<Future<String>> futureArrayList = new ArrayList<>();
        System.out.println("公司让你通知大家聚餐 你开车去接人");
        Future<String> future10 = executorService.submit(() -> {
            System.out.println("总裁：我在家上大号 我最近拉肚子比较慢 要蹲1个小时才能出来 你等会来接我吧");
            TimeUnit.SECONDS.sleep(10);
            System.out.println("总裁：1小时了 我上完大号了。你来接吧");
            return "总裁上完大号了";
        });
        futureArrayList.add(future10);
        Future<String> future3 = executorService.submit(() -> {
            System.out.println("研发：我在家上大号 我比较快 要蹲3分钟就可以出来 你等会来接我吧");
            TimeUnit.SECONDS.sleep(3);
            System.out.println("研发：3分钟 我上完大号了。你来接吧");
            return "研发上完大号了";
        });
        futureArrayList.add(future3);
        Future<String> future6 = executorService.submit(() -> {
            System.out.println("中层管理：我在家上大号  要蹲10分钟就可以出来 你等会来接我吧");
            TimeUnit.SECONDS.sleep(6);
            System.out.println("中层管理：10分钟 我上完大号了。你来接吧");
            return "中层管理上完大号了";
        });
        futureArrayList.add(future6);
        TimeUnit.SECONDS.sleep(1);
        System.out.println("都通知完了,等着接吧。");
        try {
            for (Future<String> future : futureArrayList) {
                String returnStr = future.get();
                System.out.println(returnStr + "，你去接他");
            }
            Thread.currentThread().join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
