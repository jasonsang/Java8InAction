package lambdasinaction.chap11;

import utils.SmallTool;

import java.util.concurrent.CompletableFuture;

public class TestThenApply {
    public static void main(String[] args) {
        SmallTool.printTimeAndThread("运行");
        SmallTool.printTimeAndThread("检查");
        CompletableFuture<String> inv = CompletableFuture.supplyAsync(()->{
            SmallTool.printTimeAndThread("+100");
            SmallTool.sleepMillis(100);
            return "200";
        }).thenApplyAsync(m ->{
           SmallTool.printTimeAndThread(String.format("%s",m));
            SmallTool.sleepMillis(200);
            return "";
        });

        SmallTool.printTimeAndThread(String.format("%s,back",inv.join()));
    }
}
