package lambdasinaction.chap11.test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class Calc {
    public static Integer calc(Integer para) {  
        try {  
            //模拟一个长时间的执行  
            Thread.sleep(1000);  
        } catch (InterruptedException e) {  
            e.printStackTrace();  
        }  
        return para * para;  
    }  
  
    public static void main(String[] args) throws ExecutionException, InterruptedException {
       /*CompletableFuture.supplyAsync 方法构造一个 CompletableFuture 实例，
       在 supplyAsync() 方法中，它会在一个新线程中，执行传入的参数。*/
        final CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> calc(50))
                .thenApply((i) -> Integer.toString(i))  
                .thenApply((str) -> "\"" + str + "\"")  
                .thenAccept(System.out::println);  
        future.get();  
    }  
}  