package lambdasinaction;

import utils.SmallTool;

import java.util.concurrent.CompletableFuture;

public class TestThenCompose {
    public static void main(String[] args) {
        CompletableFuture<String> completableFuture1 =
                CompletableFuture.supplyAsync(()->{
                    SmallTool.printTimeAndThread("检测");
                    SmallTool.sleepMillis(200);

                    return "检测结束";
                }).thenCompose/*thenComposeAsync*/(a->{
                   SmallTool.printTimeAndThread("A->?");
                    return CompletableFuture.supplyAsync(()->{
                       SmallTool.printTimeAndThread("B");
                       SmallTool.sleepMillis(200);
                        return a+"done";
                    });
                });

        SmallTool.printTimeAndThread(completableFuture1.join()+"go");
     }
}
