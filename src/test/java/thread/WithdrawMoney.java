package thread;

public class WithdrawMoney extends Thread {
	// 需求： 一个银行账户5000块，两夫妻一个拿着 存折，一个拿着卡，开始取钱比赛，
	// 每次只能取一千块，要求不准出现线程安全问题。
	static int money = 5000;

	WithdrawMoney(String name) {
		super(name);
	}

	@Override
	public void run() {
		while (true) {
			synchronized (WithdrawMoney.class) {
				if (money > 0) {
					System.out.println(this.getName() + "取了" + 1000 + "元");
					money = money - 1000;
				} else {
					System.out.println("取完了");
					break;
				}
			}
		}
	}
}
class WithdrawMoneyDemo {
	public static void main(String[] args) {
		WithdrawMoney t1=new WithdrawMoney("银行卡");
		WithdrawMoney t2=new WithdrawMoney("存折");
		t1.start();
		t2.start();
		
		
		
	}
	
	
}