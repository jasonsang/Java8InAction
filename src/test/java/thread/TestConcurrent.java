package thread;

import java.util.concurrent.atomic.AtomicLong;

public class TestConcurrent {
    private static final AtomicLong sq = new AtomicLong(1323);

    static long start = System.currentTimeMillis();

    public static void main(String[] args) throws InterruptedException {
        System.out.println(sq.getAndIncrement());


            Thread thread = new Thread(()->{

                try {
                    doA();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            },"thread_A");
            thread.start();
            doB();
            thread.join();
        System.out.println(System.currentTimeMillis() - start);

    }

    private static void doA() {
    }

    private static void doB() {
    }
}
