package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;


public class ExampleTimingNode<Update> {
//    private final String identifier;
//
//    private final Map<Update,Long> arrivalTime = new HashMap<>();
//
//    public ExampleTimingNode(String identifier) {
//        this.identifier = identifier;
//    }
//    public synchronized String getIdentifier(){
//        return identifier;
//    }
//
//    public synchronized boolean progagateUpdate(Update update_){
//        Long timeReceived = arrivalTime.get(update_);
//        return timeReceived != null;
//    }
//
//    final Object local = new StringBuffer("localhost:8888");
//    final Object other = new StringBuffer("localhost:9999");
//    final Update first = getUpdate("1");
//    final Update second = getUpdate("2");
//
//    private Update getUpdate(String s) {
//        return (Update) s;
//    }
    private static int threadTotal = 200;
    private static int clientTotal = 5000;
    private static long count = 0;


    public static void main(String[] args) {

        ExecutorService executorService = Executors.newCachedThreadPool();

        final Semaphore semaphore = new Semaphore(threadTotal);
        for (int i = 0; i < clientTotal; i++) {
            executorService.execute(() ->{
                try {
                    semaphore.acquire();
                    add();
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
        }

        executorService.shutdown();
        System.out.println(count);

    }

    private static void add() {
        count ++;
    }


}
