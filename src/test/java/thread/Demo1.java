package thread;

public class Demo1 {
    public static void main(String[] args) {
        new TestThread().run();
        for (int i = 0; i < 5; i++) {
            System.out.println("main线程在运行");
        }
        new TestThread2().run();
        //start()
        new TestThread().start();//只调用一次\
        //JAVA8中新方法
        //(参数)->："->"用来分隔参数和主体
        Runnable task = ()->{
            String threadName = Thread.currentThread().getName();
            System.out.println("hello"+threadName);
        };
        task.run();
        Thread thread = new Thread(task);
        thread.start();
        System.out.println("done!");
    }

    static class TestThread extends Thread {
        public void run()
        {
            for (int i = 0; i < 5; i++) {
                System.out.println("TestThread 在运行");
            }

            try{
               Thread.sleep(1000);//delay 1s
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
     static class TestThread2 implements Runnable {

         @Override
         public void run() {
             for (int i = 0; i < 5; i++) {
                 System.out.println("runnable interface is running");
             }
         }
     }
}
