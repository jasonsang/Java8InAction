package thread;

public class ThreadDemo {

	public static void main(String[] args) {
		Mthread m = new Mthread("我的第一个线程对象：");
		m.start();
		Mythread my = new Mythread("我的第二个线程对象：");
		my.start();
	}

}

class Mthread extends Thread {
	Mthread(String name) {
		super(name);
	}

	@Override
	public void run() {
		for (int i = 1; i < 21; i++) {
			synchronized ("a") {//同步 代码块     锁：一个对象，不能用不同对象锁不住
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("我在听歌" + i);// 获取线程的名字
			}
		}
	}
}

class Mythread extends Thread {
	Mythread(String name) {
		super(name);
	}

	@Override
	public void run() {
		for (int i = 1; i < 21; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("我在QQ聊天" + i);// 获取线程的名字
		}

	}
}