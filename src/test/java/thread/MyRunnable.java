package thread;

public class MyRunnable implements Runnable {//不是线程类 只是实现了Runnable接口

	@Override
	public void run() {
		for(int i=0;i<20;i++){
			System.out.println(Thread.currentThread().getName()+i);
			//获取线程的名字  this.getName() 
			//实现Runnable接口，Thread.currentThread().getName()
		}
		
	}

}
class Test{
	public static void main(String[] args) {
		MyRunnable my=new MyRunnable();//创建实现Runnable接口的类的对象
		Thread t=new Thread(my,"窗口一");//创建线程类对象，参数放Runnable，线程名字
		t.start();//运行RUN方法
	}
}
