package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestPool {
    /**
     * 线程池方便线程重用
     */
    public static void main(String[] args) {
        int thereads = 4;

        //少量线程池，不会动态新建新的线程。
        ExecutorService executorService =  Executors.newFixedThreadPool(2);

        //有大量任务执行的线程池
        //创建一个可以根据需要创建新线程池的线程池
        //删除了60s内等待线程
        ExecutorService executorService1 = Executors.newCachedThreadPool();

        /**
         * //带有返回值的线程任务通过实现Callable<T>接口，线程池执行任务并通过Future<T>
         *         //的实例获取返回值，Future表示异步计算结果，提供检查计算是否完成的方法，以等待计算的完成。
         *         //并检索计算的结果，Future的cancel方法表示取消任务的执行，cancel方法有一个布尔参数
         *   当参数为true时，表示立即终端任务的执行；反之允许其运行完成。
         *   Future的get方法表示等待计算完成，取其计算后的结果。
         */



    }
}
