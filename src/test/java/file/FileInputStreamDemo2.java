package file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileInputStreamDemo2 {

	public static void main(String[] args) {
		FileInputStream fi = null;
		FileOutputStream fo = null;
		byte[] data = new byte[5];//用数组读写
		try {
			fi = new FileInputStream("E:\\demo\\a.txt");
			fo = new FileOutputStream("E:\\demo\\c.txt");
			int length;
			while ((length = fi.read(data)) != -1) {
				System.out.println(new String(data, 0, length));
				fo.write(new String(data, 0, length).toUpperCase().getBytes());
			}
			System.out.println(length);
		} catch (FileNotFoundException e) {
			System.out.println("文件找不到，请重新检查文件是否存在");
		} catch (IOException e) {
			System.out.println("文件读取错误");
		}
		try {
			fi.close();
		} catch (IOException e) {
			System.out.println("文件关闭出现错误");
		}
		try {
			fo.close();
		} catch (IOException e) {
			System.out.println("文件关闭出现错误");
		}
	}

}
