package file;



import java.io.*;

public class StreamTest {

    public static void main(String[] args) throws IOException {
        File file = new File("/a.txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        //写入
        DataOutputStream dataOutput = new DataOutputStream(bufferedOutputStream);
        dataOutput.writeInt(65);//-->写入A
        dataOutput.close();
        fileOutputStream.close();
    }
}
