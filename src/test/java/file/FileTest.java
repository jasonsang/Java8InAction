package file;

import java.io.*;
import java.net.URL;
import java.util.stream.Collectors;

public class FileTest {

    public static void main(String[] args) throws IOException {
        FileReader fileReader1 = new FileReader("resources/File/a.txt");
      //  FileReader fileReader2 = new FileReader(new File(FileTest.class.getClassLoader().getResource("/File/a.txt").getPath()));
        BufferedReader bufferedReader = new BufferedReader(fileReader1);
        String lineStr = "";
        while((lineStr = bufferedReader.readLine()) != null) {
            System.out.println(lineStr);
        }
        fileReader1.close();
        System.out.println("读取完毕");


        URL classPath = Thread.currentThread().getContextClassLoader().getResource("");
/**
 * 解决方案：
 *
 * 不读取工程中的文件地址，直接将对应文件转化为二进制流进行操作。
 *
 */



        //InputStream inputStream = new FileInputStream(String.valueOf(is));
        //byte[] getData = readInputStream(inputStream);
        //inputStream.read(getData);
        //String str = new String(getData);
        //System.out.println ("打印内容："+str);
        InputStream is = FileTest.class.getClassLoader().getResourceAsStream("file/a.txt");

//        InputStream inputStream = null;
        String result = new BufferedReader(new InputStreamReader(is))
                .lines().collect(Collectors.joining("\n"));

        System.out.println(result);
    }

    public static  byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }

}
