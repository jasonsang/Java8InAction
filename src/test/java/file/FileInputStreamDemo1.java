package file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileInputStreamDemo1 {

	public static void main(String[] args) {
		FileInputStream fi = null;
		FileOutputStream fo =null;
		try {
			fi = new FileInputStream("E:\\demo\\a.txt");
			fo = new FileOutputStream("E:\\demo\\b.txt");
			int value, count = 0;
			// ��ȡa.txt���ļ����ݣ�ͳ�Ƴ��ļ��д�д��ĸ�ĸ���������ڿ���̨ 
			 //��a.txt�еĴ�д��ĸд��b.txt��
			while ((value = fi.read()) != -1) {
				System.out.print((char) value);
				if (value >= 65 && value <= 90) {
					fo.write(value);
					count++;
				}
			}
			System.out.println("\n"+"��д��ĸ���Ĵ����ǣ�"+count);
			System.out.println("�ļ���д���");
		} catch (FileNotFoundException e) {
			System.out.println("�ļ��Ҳ����������¼���ļ��Ƿ����");
		} catch (IOException e) {
			System.out.println("�ļ���ȡ����");
		} finally {
			try {
				fi.close();// �ر�������
			} catch (IOException e) {
				System.out.println("�ļ��رճ��ִ���");
			}
			try {
				if(fo!=null){
					fo.close();
				}
			} catch (IOException e) {
				System.out.println("�ļ��رճ��ִ���");
			}
		}
	}

}
