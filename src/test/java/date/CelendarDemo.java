package date;

import java.util.Calendar;

public class CelendarDemo {

	public static void main(String[] args) {
		// 3. 自定义一个方法，方法只有一个参数：年（例如2019）
		// a.要求返回参数年对应的每个月的最后一天的字符串数组，数组中的日期格式如：20190131, ... ,20191231
		// b.创建测试类测试，输出你得到的数组中的元素
		String[] s = getLastDay(2019);
		for (String str : s) {
			System.out.println(str);
		}
	}

	public static String[] getLastDay(int year) {
		String[] s = new String[12];
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);
		for (int i = 0; i < 12; i++) {
			c.set(Calendar.MONTH, i);
			int max = c.getActualMaximum(Calendar.DAY_OF_MONTH);
			if (i < 9) {
				s[i] = year + "" + "0" + (i + 1) + "" + max + "";
			} else {
				s[i] = year + "" + (i + 1) + "" + max + "";
			}
		}
		return s;

	}
}
