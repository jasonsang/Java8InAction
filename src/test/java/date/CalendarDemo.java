package date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarDemo {

	public static void main(String[] args) throws ParseException {
		//String-->Date
		String str="2019-7-3";
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Date d=sdf.parse(str);
		System.out.println(sdf.format(d));
		//Date--->String
		Date b=new Date();
		SimpleDateFormat s=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date1=s.format(b);
		System.out.println(date1);
		//Calendar-->Date
		Calendar c = Calendar.getInstance();
		Date date=c.getTime();
		System.out.println(date);
		getLastDay("2019","3");
		
	}
	//写入一个方法，传入一个年月，如201904，参数，返回参数年月的第一天和最后一天
	public static void getLastDay(String year,String month){
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, Integer.parseInt(year));
		c.set(Calendar.MONTH, Integer.parseInt(month)-1);
		System.out.println(c.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		
	}
}
