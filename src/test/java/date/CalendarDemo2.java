package date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarDemo2 {

	public static void main(String[] args) throws ParseException {
		String str="1991-1-5";
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Date date=sdf.parse(str);
		Calendar c=Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		Calendar c1 = Calendar.getInstance();
		c1.setTime(date);
		long diffDays = (c.getTimeInMillis() - c1.getTimeInMillis())
                / (1000 * 60 * 60 * 24);
		System.out.println(c1.getTimeInMillis());
		System.out.println(c.getTimeInMillis());
		System.out.println(c1.getTime());
		System.out.println(c.getTime());
        System.out.println("您已经活了：" + diffDays + " 天");
 
	}

}
