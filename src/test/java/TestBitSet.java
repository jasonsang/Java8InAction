import java.util.BitSet;

public class TestBitSet {

    public static void main(String[] args) {
        //很长的二进制数组可以用BitSet类型表示,来自C++
        BitSet bitSet1 = new BitSet();
        BitSet bitSet2 = new BitSet();
        bitSet1.set(1);
        bitSet2.set(1);
        bitSet2.xor(bitSet1);
        System.out.println(bitSet2);
    }
}
