import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class TestHashMap {

    public static void main(String[] args) {
        //键值对-JAVA内置对象
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("User-Agent","Chrome 8");
        System.out.println(hashMap.get("User-Agent"));
        for (Map.Entry<String, String> entry : hashMap.entrySet()) {
            System.out.println(entry.getKey()+"->"+entry.getValue());
        }

        //键值对-自定义对象
        HashMap<HttpHost,Integer> httpHostHashMap = new HashMap<>();
        httpHostHashMap.put(new HttpHost("lietu.com"),8);

    }

    private static class HttpHost {
        private String DEFAULT_SHEME_NAME;
        private final String hostName = "";
        private final String localHostName = "";
        private final int port = 0;
        private final String schemeName = "";


        public HttpHost(String s) {
            DEFAULT_SHEME_NAME = s;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof HttpHost)) return false;
            HttpHost httpHost = (HttpHost) o;
            return port == httpHost.port &&
                    Objects.equals(hostName, httpHost.hostName) &&
                    Objects.equals(localHostName, httpHost.localHostName) &&
                    Objects.equals(schemeName, httpHost.schemeName);
        }

        @Override
        public int hashCode() {
            //改写
            //int hash = HASH_SEED;
            return Objects.hash(localHostName, port, schemeName);
        }
    }
}
