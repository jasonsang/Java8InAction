package reflect;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ReflectTest {

	public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, SecurityException {
		// a.用字节流读取任意一个文本文件pathName.properties，返回读取到的文件中的内容，自定义一个如下方法实现：String
		// getFileData(File f){}
		// b. 自定义一个如下方法,用来实现根据上述方法传递的类的完整路径创建一个该类的对象并且返回：Object getObject
		// (String pathName){}
		// c.实现完用测试类进行测试 25分
		File f=new File("D:\\pathName.properties");
		String pathName=getFileData(f);
		System.out.println(getObject(pathName));
	}
	public static String getFileData(File f) throws IOException {
		String s = "";
		FileInputStream fi = null;
		fi = new FileInputStream(f);
		int value;
		while ((value = fi.read()) != -1) {
			s += (char) value;
		}
		if (fi != null) {
			fi.close();
		}
		return s;
	}

	public static Object getObject(String pathName) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, SecurityException {
		Class clazz=Class.forName(pathName);
		Constructor c=clazz.getDeclaredConstructor();
		c.setAccessible(true);
		Object o=c.newInstance();
		return  o;
	}
}
