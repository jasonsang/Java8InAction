package reflect;

public class Person {
	private String name;
	public int age;

	private Person() {
	}

	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}
	private void show(String a,int b){
		System.out.println(a+b);
	}
	public void display(){
		System.out.println("display");
	}
}
