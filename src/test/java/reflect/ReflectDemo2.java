package reflect;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectDemo2 {

	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException  {
 //6.(1)写一个a.properties格式的配置文件，配置类的完整名称(reflect.Person)
 //(2) 写一个程序，读取这个Properties配置文件，获得类的完整名称并加载这个类
 //(3)用反射 的方式创建对象运行display方法
		//1.流读取文件，String s=reflect.Student;
		FileInputStream fi = null;
		String s="";
		try {
			fi = new FileInputStream("D:\\a.properties");
			int value;
			while((value=fi.read())!=-1){
				s+=(char)value;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				if(fi!=null){
					fi.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//2.clazz=class.forname(s);
		Class clazz=Class.forName(s);
		//3.Constructor c=clazz.getDeclaredConstructor();
		Constructor c=clazz.getDeclaredConstructor();
		c.setAccessible(true);
		//4.Object o=c.newInstance();
		Object o=c.newInstance();
		Method m=  clazz.getDeclaredMethod("display");
	    m.invoke(o);
	}

}
