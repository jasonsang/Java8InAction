package reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ReflectDemo01 {

	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		/*获取Person类的Class对象的三种方式：
		 * 1.类名.class属性
		 * 2.对象.getClass()方法获取
		 * 3. Class.forName("reflect.Person")  ********** 框架 JDBC连接数据库
		 */
//		Person p=new Person();
		Class clazz=Person.class;
		Class clazz1=Person.class;
//		Class clazz2=p.getClass();
		//ClassNotFoundException 受检异常  “当前类找不到”
		Class clazz3=Class.forName("reflect.Person");
		System.out.println(clazz==clazz3);//任意一个类或者接口，对应的Class对象有一个
	
		//通过clazz对象获取Person类中的属性，构造，方法
		//clazz.getConstructors();获取Person类中public的构造
//		Constructor[] c=clazz.getConstructors();
		//clazz.getConstructors();获取Person类中所有的构造
		Constructor[] c=clazz.getDeclaredConstructors();
		//遍历数组，获取所有的构造方法
		for(Constructor con:c){
			System.out.println(con);
		}
		
		//获取Person类中private的构造
		Constructor c1=clazz.getDeclaredConstructor();
		//强制访问私有的构造方法
		c1.setAccessible(true);
		Object o=c1.newInstance();
		Object o1=c1.newInstance();
		Person p=(Person)o;
		p.setName("lisi");
		p.setAge(23);
		System.out.println(p);
		System.out.println(o1);
	}

}
/*

reflect  反射
String类，Person类。。。
1.类加载到内存里，创建了一个Class类的对象（java源文件编译为字节码文件），类中static属性分配空间，
new Person(), 非static属性分配空间
2.Class类：对象代表的是当前类的字节码文件（有一个），Class类的对象有几个？  一个

public getInstance(String className){//reflect.Student
	Class clazz=Class.forName(className);
	Constructor c=clazz.getDeclaredConstructor();
	Object o=c.newInstance();
}

new Person();
反射：程序在运行过程中动态获取类，或者对象中的属性，方法，构造等信息
首先要获取该类（Person）的Class类的对象
*/