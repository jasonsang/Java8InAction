package jdbcdemo;


import java.sql.*;
import java.util.ArrayList;

public class JDBCDemo {
	public static final String url="jdbc:mysql://localhost:3306/db_test";
	public static final String user="root";
	public static final String password="root";
	public static void main(String[] args) {
		//创建lib文件夹，将驱动包拷贝到lib中，右键，build path/add to build path
		//加载驱动类
      int row;
      Connection conn=null;
      Statement st=null;
      ResultSet rs=null;
      ArrayList<Stu> stuList=new ArrayList<Stu>();
	try {
		Class.forName("com.mysql.jdbc.Driver");
		//建立连接：和数据库的连接
		   conn=DriverManager.getConnection(url, user, password);
		//可以执行sql语句：依靠statement
		  st=conn.createStatement();
		//执行sql语句：增删改
		 // String sql="insert into tbl_stu values('09','彤彤',18)";
		  //String sql="update tbl_stu set age=23 where id='09'";
		  //String sql="delete from tbl_stu where id='09'";
		  //row = st.executeUpdate(sql);
		  //System.out.println(row);
		  String sql="select *from tbl_stu";
		  rs=st.executeQuery(sql);
		//遍历结果集，取出所有的数据，每行每列挨个取
		  while(rs.next()){
			  String sno=rs.getString(1);
			  String cno=rs.getString(2);
			  int grade=rs.getInt(3);
			  Stu stu=new Stu(sno,cno,grade);//创建对象
			  stuList.add(stu);//封装到LIST里
		  }
		  System.out.println(stuList);
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{
		try {
			if(rs!=null)
				rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(st!=null)
				st.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(conn!=null)
				conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
      
      
	}
}
