package jdbcdemo;

import java.sql.*;


public class DBUtil {
     public static final String DRIVER="com.mysql.jdbc.Driver";
     public static final String URL="jdbc:mysql://localhost:3306/test";
	 public static final String USER="root";
	 public static final String PASSWORD="root";
	//获取连接数据库对象
	public Connection getConnection(){
		Connection conn=null;
		try {
			Class.forName(DRIVER);
			conn=DriverManager.getConnection(URL,USER,PASSWORD);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
		
	}
	// 关闭各个对象
	public void closeAll(ResultSet rs,PreparedStatement pst,Connection conn){
		try {
			if(rs!=null){
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(pst!=null){
				pst.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(conn!=null){
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
