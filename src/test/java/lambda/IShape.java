package lambda;

public interface IShape {
   double PI=3.14;
	
   int getPerimeter();
   int getArea();
}
