package lambda;

interface InterfaceDemo {

}
class Pencil{//铅笔
	void write(){
		System.out.println("铅笔能写字");
	}
	
}

class PencileWithClear extends Pencil implements Clear,Clear1{
	public void clear(){
		System.out.println("铅笔能擦除");
	}
	public void show(){
		
	}
}
interface Clear1{
	void show();
}
//接口
interface Clear{
	double PI=3.14;//public static final 
	void clear();
//	{
//		System.out.println("铅笔能擦除");
//	}
}
/*

abstract 修饰的类：里面含有抽象方法（1，2...N）如果类中没有抽象方法，可以
有已经实现了的方法吗？--可以有
abstract 修饰的方法：

接口：interface 一种特殊的抽象类 接口中所有的属性和方法默认都加public
程序统一规范，便于后期程序的维护，功能扩展
1.接口中属性都是常量，默认前面加上public static final 
2.接口中方法都是抽象方法，默认前面有public abstract
3.接口中没有构造，变量，实现了的方法
4.抽象类可以new对象吗？不可以，接口可以new对象吗？--不可以

所有方法通常都定义为抽象（没有实现的方法）
开闭原则：扩展功能开发，对修改功能关闭

继承类 ：stu is person 继承好处？提高代码的重用
实现接口     stu has xxx功能

KFC 统一 clean() meeting()

java可以多实现，但是只能单继承

jdbc的时候：
增加qq用户
查询用户
修改
删除delete

insertStu()   insert()  update() delete() query()
updateStu()
deleteStu()


insertTeacher()
updateTeacher()
deleteTeacher()

interface Dao{//dao层
insert();
  update();
   delete();
    query();
}


*/