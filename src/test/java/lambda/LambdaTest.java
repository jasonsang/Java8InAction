package lambda;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * java 内置四大核心函数接口
 *
 * 1-消费型 Consumer<T> void accept(Obj obj)
 * 2-供给型Supplier<T> T get()
 * 3-函数型Function<T,R> R apply (Obj obj)
 * 4-断定型boolean test(Obj obj)
 */

public class LambdaTest {
    @Test
    public void test() {
        //do something
        happyEnd(500.01, new Consumer<Double>() {
            @Override
            public void accept(Double aDouble) {
                System.out.println("买衣服"+"花了"+aDouble);
            }
        });

        happyEnd(60.00,money -> System.out.println("吃顿饭"+"花了"+money));

    }
    public void happyEnd(Double money, Consumer<Double> consumer){
        consumer.accept(money);
    }

    @Test
    public void test2() {
        //do something
        List<String> list = Arrays.asList("北京", "大连", "天津");
        List<String> filterStrs = filterString(list, new Predicate<String>() {
            @Override
            public boolean test(String s) {
                //查包含字段
                return s.contains("大连");
            }
        });
        System.out.println(filterStrs);

        List<String> filterStrs2= filterString(list, s->s.contains("北京"));
        System.out.println("--"+filterStrs2);
    }
        public List<String> filterString (List < String > list, Predicate < String > predicate){
            ArrayList<String> arrayList = new ArrayList<>();
            for (String s: list) {
                //obj do something...
                if (predicate.test(s)) {
                    arrayList.add(s);
                }
            }
            return arrayList;
        }

    }