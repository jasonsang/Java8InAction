package lambda;

/**
 * output:=5 因为this只的是包含他的Runnable而不是主类对象(就近取对象)
 */
public class InClass {
    private static int value = 0;


    public static class MeaningOfThis<value> {
        InClass aClass = new InClass();
        public void doIt(){
            InClass aClass1 = this.aClass;
            value =1;
            aClass1.value=2;
            System.out.println(value);

            Runnable r = new Runnable() {
                public final int value = 5;
                public void run(){
                    int value = 10;

                    System.out.println(this.value);
                }
            };
            r.run();
        }
        public static void main(String[] args){
            MeaningOfThis m = new MeaningOfThis();
            m.doIt();

        }
    }
}
