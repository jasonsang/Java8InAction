package lambda;

public class InterfaceShape {

}
class Rectangle2 extends InterfaceShape implements IShape{
	int a;
	int b;
	public Rectangle2() {

	}

	public Rectangle2(int a, int b) {
		this.a = a;
		this.b = b;
	}
	public int getPerimeter() {
		int l;
		l = 2 * (a + b);
		return l;
	}

	public int getArea() {
		int s;
		s = a * b;
		return s;
	}
}
class Circle2 extends InterfaceShape implements IShape {
	int r;
	public Circle2() {
		
	}
	
	public Circle2(int r) {
		this.r = r;
	}
	public int getPerimeter() {
		double l;
		l = 2 * PI * r;
		return (int) l;
	}
	
	public int getArea() {
		double s ;
		s = PI * r * r;
		return (int) s;
	}
}

class T22{
	public static void main(String[] args) {
		Rectangle2 a = new Rectangle2(3,4);
		Circle2 b = new Circle2(10);
		System.out.println(a.getArea());
		System.out.println(a.getPerimeter());
		System.out.println(b.getArea());
		System.out.println(b.getPerimeter());
	}
}
