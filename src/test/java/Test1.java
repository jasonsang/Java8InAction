import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class Test1 {
    @Test
    public void debug() {
        // 数据源
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);

        list.stream()
                // 过滤出大于3的数据项 - 将Lambda表达式拆分为多行形式
                .filter(item -> {
                    // 获取比较结果
                    boolean test = item > 3;
                    // 返回比较结果
                    return test;
                })
                // 打印每个数据项
                .forEach(System.out::println);
    }

    @Test
    public void debug2() {
        // 数据源
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);

        list.stream()
                // 过滤出大于3的数据项
                .filter(item -> item > 3)
                // 打印filter之后的结果
                .peek(System.out::println)
                // 打印每个数据项
                .forEach(System.out::println);
    }

}
