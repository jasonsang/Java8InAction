package dom;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class TestDOM {
    public static void main(String[] args) throws IOException {
        String url = "http://politics.people.com.cn/GB/1024/";
        //timeout 超时等待时间 ms
        Document document = Jsoup.connect(url).timeout(1000).get();
        //System.out.println(document.html());
        Elements element = document.getElementsByClass("list_16");
        Elements links = element.select("a[href]");
        for (Element link : links) {
            //coding here
            String title = link.text();
            System.out.println(title);
        }

    }
}
